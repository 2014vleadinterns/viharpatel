import sys 

__all__ = ["test","i"]

i = 0
fp = open("testfile", "r")
text = fp.readlines()
nlines = len(text)
for i in range(0, nlines):
    text[i] = text[i].strip('\n')

#string = []

def test(s):
    a = s.split()
    if(len(a)):
	if(len(a)!=4):
	    return ("Syntax Error in line:" + str(i+1))
	elif(a[0]!='+' and a[0]!='/' and a[0]!='-' and a[0]!='*' and a[0]!='%'):
	    return ("Error in OPERANDS' Column in line:" + str(i+1))
	elif (not a[3].isdigit()):
	    return ("OK")
	elif((a[1].isdigit() and a[2].isdigit()) != a[3].isdigit()):
	    #if(a[1].isdigit() and a[2].isdigit()):
	#	print "Result should be a NUMBER in line:", i+1
	    if(a[3].isdigit()):
		return ("Result should be a VARIABLE in line:" + str(i+1))
	    else:
		return ("OK")
	else:
	    return ("OK")
	    
    else:
	return ("OK")


for i in range(0, nlines):
    #print a
    print test(text[i])

