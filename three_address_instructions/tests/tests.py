import os
import sys
sys.path.append(os.path.abspath(os.pardir)+"/src")


from three_adr_inst import *

def test_length():
    t = test("+")
    assert(t == "Syntax Error in line:" + str(i+1))
    t = test("")
    assert(t == "OK")
    t = test("+ a")
    assert(t == "Syntax Error in line:" + str(i+1))
    t = test("+ a b")
    assert(t == "Syntax Error in line:" + str(i+1))
    t = test("+ a b c")
    print t
    assert(t == "OK")

def test_operands():
    t = test("+ a b c")
    assert(t == "OK")
    t = test("- a b c")
    assert(t == "OK")
    t = test("% a b c")
    assert(t == "OK")
    t = test("/ a b c")
    assert(t == "OK")
    t = test("* a b c")
    assert(t == "OK")
    t = test("< a b c")
    assert(t == "Error in OPERANDS' Column in line:" + str(i+1))
    t = test("_ a b c")
    assert(t == "Error in OPERANDS' Column in line:" + str(i+1))
    t = test("** a b c")
    assert(t == "Error in OPERANDS' Column in line:" + str(i+1))

def test_all():
    t = test("+ a b 10")
    assert(t == "Result should be a VARIABLE in line:" + str(i+1))
    t = test("% 10 b c")
    assert(t == "OK")
    t = test("/ 13 4 c")
    assert(t == "OK")
    t = test("- 10 b 100")
    assert(t == "Result should be a VARIABLE in line:" + str(i+1))
    t = test("* 20 20 312")
    assert(t == "OK")

test_length()
test_operands()
test_all()
